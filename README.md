# OSI² Build Workshop | PTB, Berlin, 22.09.2023

On September 22 members of the Open Source Imaging Initiative (OSI²) came together for a hybrid build workshop to share their knowledge in order to facilitate local rebuilds of the open source low-field MRI scanner "OSI² ONE".
The presentations aim to be generally understandable (for the target group) and discuss design rationale, personal experience from rebuilds, workarounds, hacks and test scenarios.

[**Blog post**](https://www.opensourceimaging.org/2023/11/21/osi%c2%b2-build-workshop-recording-published-%f0%9f%8e%89/)

## Agenda

| Time  UTC+2/CEST | Session Title                                           | Speaker on-site, virtual |
|------------------|---------------------------------------------------------|--------------------------|
| 08:00 - 08:30    | Opening & Short Introduction of Participants            | Lukas Winter, all        |
| 08:30 - 09:00    | Communication & Contribution Management                 | Martin Häuer             |
|                  | **Magnet**                                              |                          |
| 09:30 - 10:00    | Main Magnet Construction                                | Lukas Winter             |
| 10:00 - 10:30    | Passive B0 Shimming                                     | Tom O’ Reilly            |
| 10:30 - 10:45    | Coffee Break                                            |                          |
|                  | **Amplifier**                                           |                          |
| 10:45 - 11:15    | Gradient Amplifier                                      | Danny de Gans            |
| 11:15 - 11:45    | RF Power Amplifier                                      | Ruben Pellicer Guiridi   |
| 11:45 - 13:00    | Lunch                                                   |                          |
|                  | **Gradient Coils**                                      |                          |
| 13:00 - 13:30    | Gradient Coil Simulations - CoilGen                     | Sebastian Littin         |
| 13:30 - 14:00    | Gradient Coil Construction and Mounting                 | Wouter Teeuwisse         |
|                  | RF chain                                                |                          |
| 14:00 - 14:30    | RF Coils                                                | Tom O’Reilly             |
| 14:30 - 15:00    | TR Switches and Preamps                                 | Marcus Prier             |
| 15:00 - 15:15    | Coffee Break                                            |                          |
|                  | Console                                                 |                          |
| 15:15 - 15:45    | Scanner control Software - Scanhub                      | David Schote             |
| 15:45 - 16:15    | Console hardware - Red Pitaya and OCRA1                 | Jan Frintz               |
| 16:15 - 16:45    | Console software - MaRCoS                               | Benjamin Menküc          |
| 16:45 - 17:15    | Pulse sequence programming - PyPulseq                   | Patrick Schüncke         |
|                  |                                                         |                          |
|                  | **Other**                                               |                          |
| 17:15 - 17:45    | Portability, Safety, Power Management etc. (discussion) | all                      |
| 17:45 - 18:00    | Wrap-up and Closing                                     | Lukas Winter             |

## Slides

Find the slides here: https://doi.org/10.5281/zenodo.10079661

## Footage

The entire session has been uploaded to YouTube

[![OSI² Build Workshop | PTB, Berlin, 22.09.2023 | Recording](https://img.youtube.com/vi/1q0dXg17E_w/0.jpg)](https://www.youtube.com/watch?v=1q0dXg17E_w "OSI² Build Workshop | PTB, Berlin, 22.09.2023 | Recording")

Raw material (pictures and video) has been uploaded here: https://doi.org/10.5281/zenodo.10144638

